import requests
import unittest


class TestLogin(unittest.TestCase):
    '''
    Before running this test module please create following user using
    MongoDB console:
        + username: vahid
        + password: test
    '''

    def test_login_success(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'vahid',
                'password': 'test'
            }
        )
        self.assertEqual('', r.json()['err'])

    def test_login_wrong_username(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'majid',
                'password': 'test'
            }
        )
        self.assertEqual(404, r.status_code)

    def test_login_wrong_password(self):
        r = requests.post(
            'http://localhost:4000/user/login',
            data={
                'username': 'vahid',
                'password': 'wrong password'
            }
        )
        self.assertEqual(404, r.status_code)

    # Test if requesting for a forget code works - existing mobile
    def test_send_forget_code_with_sms_for_existing_mobile(self):
        r = requests.post(
            'http://localhost:4000/user/send-code-with-sms'
            data={
                'mobile': '09121235678'
            }
        )

        self.assertEqual(200, r.status_code)

    # Test if requesting for a forget code works - non existing mobile
    def test_send_forget_code_with_sms_for_non_existing_mobile(self):
        r = requests.post(
            'http://localhost:4000/user/send-code-with-sms'
            data={
                'mobile': '08151232134'
            }
        )

        self.assertEqual(404, r.status_code)

    # Test if chaning the password with the recived code works
    def test_change_password_with_correct_sms_code(self):
        r = requests.post(
            'http://localhost:4000/user/change-password-with-sms'
        )
        data={
            'code': '1234',
            'new_password': 'qwer'
        }
        self.assertEqual(200, r.status_code)
        

    def test_change_password_with_wrong_sms_code(self):
        r = requests.post(
            'http://localhost:4000/user/change-password-with-sms'
        )
        data={
            'code': '3456',
            'new_password': 'qwer'
        }
        self.assertEqual(404, r.status_code)
        
if __name__ == '__main__':
    unittest.main()
