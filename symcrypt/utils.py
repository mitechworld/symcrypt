from werkzeug.contrib.cache import SimpleCache
import random

from user.models import UserModel


cache = SimpleCache()

def generate_code():
    return random.randit(1111,9999)

def send_sms(mobile, text):
    # send_sms uses another service to send a sms
    pass

def send_and_cache_code(mobile):
    code = generate_code()
    r = send_sms(mobile, str(code))
    if r == '200':
        cache.set(str(code), mobile, timeout=5 * 60)
        return 1
    else:
        return 0


def get_user_with_code(code):
    mobile = cache.get(str(code))
    user = UserModel.objects(mobile=mobile).first()

    return user



