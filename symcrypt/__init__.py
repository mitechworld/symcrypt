import importlib
import inspect

from flask import Flask, session, g, request, jsonify, abort, url_for
from flask_login import LoginManager, login_user
from flask_restful import Api

from mongoengine import connect
from symcrypt.user.models import UserModel
from symcrypt.conf import MONGO_HOST, MONGO_PORT, SECRET_KEY, INSTALLED_MODULES
import utils

connect('symcrypt', host=MONGO_HOST, port=MONGO_PORT)


app = Flask(__name__)
app.secret_key = SECRET_KEY

login_manager = LoginManager()
login_manager.init_app(app)

api = Api(app)


@login_manager.user_loader
def load_user(user):
    return UserModel.objects(username=user).first()


@app.route('/user/login', methods=['POST'])
def login():
    u = UserModel.objects(username=request.form['username']).first()
    if u:
        if u.password == request.form['password']:
            login_user(u) 
            return jsonify(
                {
                    'err': '',
                }
            ), 200
        else:
            return jsonify(
                {
                    'err': 'username or password is not correct',
                }
            ), 400
    else:
        return jsonify(
            {
                'err': 'the user does not exist'
            }
        ), 404

@app.route('/user/send-code-with-sms', methods=['POST'])
def send_token_with_sms():
    mobile = request.form['mobile']
    u = UserModel.objects(mobile=mobile).first()
    if not u:
        return jsonify(
            {
            'err': 'mobile not found!'
            }
        ), 404


    utils.send_and_cache_code(mobile)
    
    return jsonify(
            {
            'err': ''
            }
        ), 200

@app.route('change-password-with-sms')
def change_password_with_sms()
    code = request.form['code']
    new_password = request.form['new_password']

    user = get_user_with_code(code)
    if not user:
        return jsonify(
            {
            'err': 'Code is not correct'
            }
        ), 404

    user.password = new_password
    user.save()
    
    return jsonify(
            {
            'err': ''
            }
        ), 200

for pkg in INSTALLED_MODULES:
    service_script = importlib.import_module(
        "symcrypt.%s.resources" % pkg)
    admin_script = importlib.import_module('symcrypt.%s.models' % pkg)
    for name, obj in inspect.getmembers(service_script):
        if inspect.isclass(obj) and 'symcrypt.%s' % pkg in str(obj) and 'Resource' in str(obj):
            api.add_resource(
                obj,
                '/%s/%s' % (pkg, obj.url),
                endpoint='%s.%s' % (pkg, obj.__name__)
            )
    for name, obj in inspect.getmembers(admin_script):
        if inspect.isclass(obj) and 'api.%s' % pkg in str(obj) and 'Model' in str(obj):
            admin.add_view(ModelView(obj))
